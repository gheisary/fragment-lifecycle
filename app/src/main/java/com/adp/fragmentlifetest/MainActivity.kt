package com.adp.fragmentlifetest

import android.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle


class MainActivity : AppCompatActivity(), FragmentManager.OnBackStackChangedListener {
    override fun onBackStackChanged() {
        val fragment = fragmentManager.findFragmentById(R.id.mainFragmentContainer)
        fragment.onResume()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var fragmentManager = getFragmentManager()
        fragmentManager.addOnBackStackChangedListener(this)
        fragmentManager.beginTransaction()
            .add(R.id.mainFragmentContainer, Fragment1.newInstance(1), "First")
            .addToBackStack("First")
            .commit()
    }
}
