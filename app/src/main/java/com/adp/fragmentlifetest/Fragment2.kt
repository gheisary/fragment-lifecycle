package com.adp.fragmentlifetest

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_1.*

class Fragment2 : Fragment() {
    companion object {
        private const val ARG_FRAGMENT_HOME = "home_fragment_argument"
        fun newInstance(someInt: Int): Fragment2 {
            val myFragment = Fragment2()
            val args = Bundle()
            args.putInt(ARG_FRAGMENT_HOME, someInt)
            myFragment.arguments = args
            return myFragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        Log.i("testFragmentLife" , "onAttach   on F2")
    }

    override fun onStart() {
        super.onStart()
        Log.i("testFragmentLife" , "onStart   on F2")
    }

    override fun onPause() {
        super.onPause()
        Log.i("testFragmentLife" , "onPause   on F2")
    }

    override fun onStop() {
        super.onStop()
        Log.i("testFragmentLife" , " onStop  on F2")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.i("testFragmentLife" , " onDestroyView  on F2")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("testFragmentLife" , "onDestroy   on F2")
    }

    override fun onDetach() {
        super.onDetach()
        Log.i("testFragmentLife" , " onDetach  on F2")
    }

    override fun onResume() {
        super.onResume()
        Log.i("testFragmentLife" , " onResume  on F2")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_2, container, false)
    }

}
