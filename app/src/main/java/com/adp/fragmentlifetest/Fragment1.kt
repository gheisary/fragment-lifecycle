package com.adp.fragmentlifetest

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_1.*

class Fragment1 : Fragment() {
    companion object {
        private const val ARG_FRAGMENT_HOME = "home_fragment_argument"
        fun newInstance(someInt: Int): Fragment1 {
            val myFragment = Fragment1()
            val args = Bundle()
            args.putInt(ARG_FRAGMENT_HOME, someInt)
            myFragment.arguments = args
            return myFragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        Log.i("testFragmentLife" , "onAttach  ------- F1")
    }

    override fun onStart() {
        super.onStart()
        Log.i("testFragmentLife" , "onStart  ------- F1")
    }

    override fun onPause() {
        super.onPause()
        Log.i("testFragmentLife" , "onPause  ------- F1")
    }

    override fun onStop() {
        super.onStop()
        Log.i("testFragmentLife" , " onStop ------- F1")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.i("testFragmentLife" , " onDestroyView ------- F1")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("testFragmentLife" , "onDestroy  ------- F1")
    }

    override fun onDetach() {
        super.onDetach()
        Log.i("testFragmentLife" , " onDetach ------- F1")
    }

    override fun onResume() {
        super.onResume()
        Log.i("testFragmentLife" , " onResume ------- F1")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_1, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnGoTo2.setOnClickListener {
            val fragmentManager = activity!!.getFragmentManager()
            fragmentManager.beginTransaction()
                .add(R.id.mainFragmentContainer, Fragment2.newInstance(1), "Fragment2")
                .addToBackStack("Fragment2")
                .commit()
        }
    }
}
